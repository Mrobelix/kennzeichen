<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class NumberplateModel
{
    protected $db;

    protected $table = 'numberplates';

    protected $allowedFields = [
        'shortcut',
        'origin',
        'district',
        'state'
    ];

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    /**
     * Funktion zum holen der Shortcuts aller Nummernschilder.
     * @return mixed
     */
    public function getShortcuts()
    {
        return $this->db->table('numberplates')
			->select('shortcut')
            ->orderBy('shortcut', 'ASC')
            ->get()
            ->getResult();
    }

    /**
     * Funktion zum holen der Bezirke aller Nummernschilder.
     * @return mixed
     */
    public function getOrigins()
    {
        return $this->db->table('numberplates')
            ->select('origin')
            ->orderBy('origin', 'ASC')
            ->get()
            ->getResult();
    }

    /**
     * Funktion zum holen der Bezirke aller Nummernschilder.
     * @return mixed
     */
    public function getDistricts()
    {
        return $this->db->table('numberplates')
			->select('district')
            ->orderBy('district', 'ASC')
            ->get()
            ->getResult();
    }

    /**
     * Funktion zum holen der Bundesländer aller Nummernschilder.
     * @return mixed
     */
    public function getStates()
    {
        return $this->db->table('states')
            ->where('shortcut != ""')
            ->orderBy('state', 'ASC')
            ->get()
            ->getResult();
    }

    /**
     * Funktion zum holen der abkürzung des eingegebenen Bundeslandes.
     * @return mixed
     */
    public function getShortState(string $state)
    {
        $query = $this->db->table('states')
            ->where('state', $state)
            ->get()
            ->getRow();

        if ($query) {
            return $query->shortcut;
        }
        return "";
    }

    /**
     * Funktion zum holen aller Nummernschilder.
     * @return mixed
     */
    public function getNumberplates()
    {
        return $this->db->table('numberplates')
			->select('numberplates.shortcut, numberplates.origin, numberplates.district, numberplates.state AS shortstate, states.state')
			->join('states', 'numberplates.state = states.shortcut')
            ->orderBy('numberplates.shortcut', 'ASC')
            ->get()
            ->getResult();
    }

    /**
     * Funktion zum Hinzufügen eines Nummernschilds (mit Shortcut, Origin, District, State)
     * @param string $shortcut
     * @param string $origin
     * @param string $district
     * @param string $state
     * @return mixed
     */
    public function addNumberplate(string $shortcut, string $origin, string $district, string $state)
    {
        return $this->db->table('numberplates')
            ->insert([
                "shortcut" => $shortcut,
                "origin" => $origin,
                "district" => $district,
                "state" => $state
            ]);
    }

    /**
     * Funktion zum Aktualisieren eines Nummernschilds (mit Shortcut, Origin, District, State)
     * @param string $shortcut
     * @param string $origin
     * @param string $district
     * @param string $state
     * @return mixed
     */
    public function updateNumberplate(string $shortcut, string $origin, string $district, string $state)
    {
        $Data = array();

        if (isset($origin)) {
            $Data["origin"] = $origin;
        }

        if (isset($district)) {
            $Data["district"] = $district;
        }

        if (isset($state)) {
            $Data["state"] = $state;
        }

        if (!empty($Data)) {
            return $this->db->table('numberplates')
                ->where('shortcut', $shortcut)
                ->update($Data);
        }
    }

    /**
     * Funktion zum Löschen eines Nummernschilds (mit Shortcut)
     * @param string $shortcut
     * @return mixed
     */
    public function deleteNumberplate(string $shortcut)
    {
        return $this->db->table('numberplates')
            ->where('shortcut', $shortcut)
            ->delete();
    }

    /**
     * Funktion zum Überprüfen, ob ein Nummernschild existiert. (mit Shortcut)
     * @param string $shortcut
     * @return mixed
     */
    public function existNumberplate(string $shortcut)
    {
        $query = $this->db->table('numberplates')
            ->selectCount('shortcut')
            ->where('shortcut', $shortcut)
            ->get()
            ->getRow();

        if ($query->shortcut == 0) {
            return true;
        }
        return false;
    }
}