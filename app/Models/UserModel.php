<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';

    protected $allowedFields = [
        'name',
        'email',
        'password',
        'token',
        'permissions',
        'created_date',
        'last_login'
    ];
}