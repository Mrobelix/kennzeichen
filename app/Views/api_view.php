<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        <?php echo $bc_pretitle; ?>
                    </div>
                    <h2 class="page-title">
                        <?php echo $bc_title; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-deck row-cards">
			
				<!-- <div id="numberplates"> -->
					<!-- <numberplate-card> -->
						<div class="col-sm-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Seite wird geladen<span class="animated-dots"></span></h3>
								</div>
								<div class="card-body">
									<p>Die Kennzeichenliste wird gerade geladen!</p>
									<div class="spinner-border" role="status"></div>
								</div>
							</div>
						</div>
					<!-- </numberplate-card> -->
				<!-- </div> -->
				
            </div>
        </div>
	</div>
		
	<!-- <script src="<?php //echo base_url('dist/js/vue/numberplates.js'); ?>"></script> -->