<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        <?php echo $bc_pretitle; ?>
                    </div>
                    <h2 class="page-title">
                        <?php echo $bc_title; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-tight py-4">
            <div class="empty">
                <div class="empty-header"><?php echo $bc_title; ?></div>
                <p class="empty-title">Oops… Sie haben gerade eine Fehlerseite gefunden</p>
                <?php
                if ($bc_title == 401) {
                    ?>
                    <p class="empty-subtitle text-muted">
                        Es tut uns leid, aber Sie haben nicht die benötigten Rechte um diese Seite aufzurufen!
                    </p>
                    <?php
                } else if ($bc_title == 404) {
                    ?>
                    <p class="empty-subtitle text-muted">
                        Es tut uns leid, aber die von Ihnen gesuchte Seite wurde nicht gefunden!
                    </p>
                    <?php
                } else {
                    ?>
                    <p class="empty-subtitle text-muted">
                        Es tut uns leid, aber dieser Fehlercode ist nicht eingetragen!
                    </p>
                    <?php
                }
                ?>
                <div class="empty-action">
                    <a href="<?php echo site_url('/'); ?>" class="btn btn-primary">
                        <img src="/static/icons/arrow-left.svg" width="24" height="24" alt="Startseite" class="icon">
                        Startseite
                    </a>
                </div>
            </div>
        </div>