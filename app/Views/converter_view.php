<?php
$session = session();
?>
<div class="page-wrapper">
    <div id="converter">
        <converter-card  pretitle="<?php if (isset($bc_pretitle)) {
            echo $bc_pretitle;
        } else {
            echo "none";
        } ?>" title="<?php if (isset($bc_title)) {
            echo $bc_title;
        } else {
            echo "none";
        } ?>" permissions="<?php if ($session->get('auth_permissions')) {
            echo $session->get('auth_permissions');
        } else {
            echo "none";
        } ?>" alert_type="<?php if ($session->get('alert_type') != 0) {
            echo $session->get('alert_type');
        } else {
            echo "none";
        } ?>" alert_title="<?php if ($session->get('alert_title') != 0) {
            echo $session->get('alert_title');
        } else {
            echo "none";
        } ?>" alert_text="<?php if ($session->get('alert_text') != 0) {
            echo $session->get('alert_text');
        } else {
            echo "none";
        } ?>">

            <!-- Body Head -->
            <div class="container-xl">
                <div class="page-header d-print-none">
                    <div class="row align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <div class="page-pretitle">
                                <?php echo $bc_pretitle; ?>
                            </div>
                            <!-- Page title -->
                            <h2 class="page-title">
                                <?php echo $bc_title; ?>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Loading -->
            <div class="page-body">
                <div class="container-xl">
                    <div class="col-sm-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Seite wird geladen<span class="animated-dots"></span></h3>
                            </div>
                            <div class="card-body">
                                <p>Die Kennzeichenliste wird gerade geladen!</p>
                                <div class="spinner-border" role="status"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </converter-card>
    </div>
</div>

<script src="<?php echo base_url('dist/js/vue/converter.js'); ?>"></script>