<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        <?php echo $bc_pretitle; ?>
                    </div>
                    <h2 class="page-title">
                        <?php echo $bc_title; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <?php
        $session = session();

        if ($session->get('alert_type')) {
            ?>

            <div class="container-xl">
                <div class="alert alert-dismissible <?php echo $session->get('alert_type'); ?>" role="alert">
                    <div class="d-flex">
                        <div>
                            <h4 class="alert-title"><?php echo $session->get('alert_title'); ?></h4>
                            <div class="text"><?php echo $session->get('alert_text'); ?></div>
                        </div>
                    </div>
                    <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
                </div>
            </div>

            <?php

            $session->remove('alert_type');
            $session->remove('alert_title');
            $session->remove('alert_text');

        }
        ?>
        <div class="container-xl">
            <div class="row row-deck row-cards">
                <div class="col-sm-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Kennzeichenliste</h3>
                        </div>
                        <div class="card-body">
                            <p><b>Diese Webseite ist eine Schulprojekt für den Differenzialunterricht in Programmiertechnik an der Heinrich-Emanuel-Merck Berufsschule.</b></p>
                            <p><b>Diese Webseite wurde von Kevin, Enrico und Gregor erstellt und Verwaltet!</b></p>
                            <br/>
                            <p><b>Erklärung:</b></p>
                            <p>Auf dieser Webseite können alle Deutschen KFZ-Kennzeichen eingesehen werden. Wir haben alle Kennzeichen in einer Kompakten Liste aufgezählt.</p>
                            <p>Außerdem kann ganz einfach und Bequem die Umwandlung genutzt werden, um aus einem Ortskürzel oder einem Zulassungsbezirk<br/> die dazugehörigen Informationen zu erhalten.</p>
                            <p>Beiden Funktionen ermöglicht es direkt über einen Button auf Google Maps oder Wikipedia weitergeleitet zu werden.</p>
                            <p>Die Kennzeichen Liste kann auch einfach heruntergeladen werden, dazu ist es nur notwendig einen Account anzulegen. Dann wird der Button angezeigt!<br/> Als Formate sind CSV, JSON oder XML vorgegeben.</p>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-xl-4">
                    <a class="card card-link" href="<?php echo site_url('numberplates'); ?>">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <span class="avatar rounded" style="background-image: url(<?php echo site_url('static/icons/grid-dots.svg'); ?>)"></span>
                                </div>
                                <div class="col">
                                    <div class="font-weight-medium">Alle Nummernschilder</div>
                                    <div class="text">Anzeigen aller Nummernschilder!</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 col-xl-4">
                    <a class="card card-link" href="<?php echo site_url('converter'); ?>">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <span class="avatar rounded" style="background-image: url(<?php echo site_url('static/icons/exchange.svg'); ?>)"></span>
                                </div>
                                <div class="col">
                                    <div class="font-weight-medium">Umwandlung</div>
                                    <div class="text">Ortskürzel und Zulassungsbezirk umwandeln!</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4 col-xl-4">
                    <a class="card card-link" href="<?php echo site_url('api'); ?>">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-auto">
                                    <span class="avatar rounded" style="background-image: url(<?php echo site_url('static/icons/api.svg'); ?>)"></span>
                                </div>
                                <div class="col">
                                    <div class="font-weight-medium">API</div>
                                    <div class="text">Informationen zur API!</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>