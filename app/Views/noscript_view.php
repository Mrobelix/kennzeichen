<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        <?php echo $bc_pretitle; ?>
                    </div>
                    <h2 class="page-title">
                        <?php echo $bc_title; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sie haben JavaScript in Ihrem Browser deaktiviert!</h3>
                    </div>
                    <div class="card-body">
                        <p>
                            Heutzutage beinhalten nahezu alle Webseiten JavaScript, eine Skriptsprache welche in Ihrem Browser läuft. Sie hilft dabei Webseiten für bestimmte Zwecke funktional zu gestalten. Sollte diese Sprache nun deaktiviert sein,
                            könnten Ihnen einige
                            Funktionen einer Webseiten nicht zur Verfügung stehen.
                        </p>
                        <div class="card-action">
                            <a href="https://www.enable-javascript.com/de" class="btn btn-primary">
                                <!-- Download SVG icon from http://tabler-icons.io/i/arrow-left -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                    <line x1="5" y1="12" x2="19" y2="12"/>
                                    <line x1="5" y1="12" x2="11" y2="18"/>
                                    <line x1="5" y1="12" x2="11" y2="6"/>
                                </svg>
                                Wie aktiviere ich Javascript?
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>