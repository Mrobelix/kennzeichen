<div class="page-wrapper">
    <?php
    $session = session();

    if ($session->get('alert_type')) {
        ?>

        <div class="page-body">
            <div class="container-xl">
                <div class="alert alert-dismissible <?php echo $session->get('alert_type'); ?>" role="alert">
                    <div class="d-flex">
                        <div>
                            <h4 class="alert-title"><?php echo $session->get('alert_title'); ?></h4>
                            <div class="text"><?php echo $session->get('alert_text'); ?></div><br/>
                            <div class="text">Bitte versuchen Sie es erneut!</div>
                        </div>
                    </div>
                    <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
                </div>
            </div>
        </div>

        <?php

        $session->remove('alert_type');
        $session->remove('alert_title');
        $session->remove('alert_text');

    }
    ?>

    <div class="page-body">
        <div class="container-tight py-4">
            <div class="text-center mb-4">
                <a href=".." class="navbar-brand navbar-brand-autodark"><img src="./static/logo.svg" height="36" alt=""></a>
            </div>
            <form class="card card-md" action="<?php echo site_url('loginaction'); ?>" method="post">
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Ins Konto anmelden</h2>
                    <div class="mb-3">
                        <label class="form-label">E-Mail Adresse</label>
                        <input id="email" name="email" type="email" class="form-control" placeholder="E-Mail Adresse eingeben..." autocomplete="off" required>
                    </div>
                    <div class="mb-2">
                        <label class="form-label">
                            Passwort
                            <span class="form-label-description">
                  <a href="<?php echo site_url('forgotpassword'); ?>">Passwort vergessen?</a>
                </span>
                        </label>
                        <div class="input-group input-group-flat">
                            <input id="password" name="password" type="password" class="form-control" placeholder="Passwort eingeben..." autocomplete="off" required>
                            <span class="input-group-text">
                  <a href="#" class="link-secondary" title="" data-bs-toggle="tooltip" data-bs-original-title="Passwort anzeigen/ausblenden" onclick="showPassword()">
                      <img id="password_eye" src="<?php echo site_url('static/icons/eye.svg'); ?>" width="24" height="24" alt="eye" class="icon">
                  </a>
                </span>
                        </div>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary w-100">Anmelden</button>
                    </div>
                </div>
            </form>
            <div class="text-center text-muted mt-3">
                Sie haben noch kein Konto? Zur <a href="<?php echo site_url('register'); ?>" tabindex="-1">Registrierung</a>!
            </div>
        </div>
    </div>

    <script>
        function showPassword() {
            var password = document.getElementById("password");
            var password_eye = document.getElementById("password_eye");

            if (password.type === "password") {
                password.type = "text";
                password_eye.src = "./static/icons/eye-off.svg";
            } else {
                password.type = "password";
                password_eye.src = "./static/icons/eye.svg";
            }
        }
    </script>