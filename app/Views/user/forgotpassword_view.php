<div class="page-wrapper">
    <div class="page-body">
        <div class="container-tight py-4">
            <div class="text-center mb-4">
                <a href=".." class="navbar-brand navbar-brand-autodark"><img src="./static/logo.svg" height="36" alt=""></a>
            </div>
            <form class="card card-md" action=".." method="get">
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Passwort vergessen</h2>
                    <p class="text-muted mb-4">Geben Sie Ihre E-Mail Adresse ein, ihr Passwort wird zurückgesetzt und Ihnen per E-Mail zugeschickt.</p>
                    <div class="mb-3">
                        <label class="form-label">E-Mail Adresse</label>
                        <input type="email" class="form-control" placeholder="E-Mail Adresse eingeben...">
                    </div>
                    <div class="form-footer">
                        <a href="#" class="btn btn-primary w-100">
                            <img src="<?php echo site_url('static/icons/mail.svg'); ?>" width="24" height="24" alt="mail" class="icon">
                            Neues Passwort senden
                        </a>
                    </div>
                </div>
            </form>
            <div class="text-center text-muted mt-3">
                Wissen Sie ihr Passwort? Zur <a href="<?php echo site_url('login'); ?>">Anmeldung</a>!
            </div>
        </div>
    </div>