<div class="page-wrapper">
    <?php
    $session = session();

    if ($session->get('alert_type')) {
    ?>

    <div class="page-body">
        <div class="container-xl">
            <div class="alert alert-dismissible <?php echo $session->get('alert_type'); ?>" role="alert">
                <div class="d-flex">
                    <div>
                        <h4 class="alert-title"><?php echo $session->get('alert_title'); ?></h4>
                        <div class="text"><?php echo $session->get('alert_text'); ?></div><br/>
                        <div class="text">Fehler:</div>
                        <?php
                        foreach ($session->get('alert_errors') as $error) {
                            echo '<div class="text">'. $error .'</div>';
                        }
                        ?>
                    </div>
                </div>
                <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            </div>
        </div>
    </div>

    <?php

        $session->remove('alert_type');
        $session->remove('alert_title');
        $session->remove('alert_text');
        $session->remove('alert_errors');

    }
    ?>

    <div class="page-body">
        <div class="container-tight py-4">
            <div class="text-center mb-4">
                <a href=".." class="navbar-brand navbar-brand-autodark"><img src="./static/logo.svg" height="36" alt=""></a>
            </div>
            <form class="card card-md" action="<?php echo site_url('registeraction'); ?>" method="post">
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Neues Konto erstellen</h2>
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input id="name" name="name" type="text" class="form-control" placeholder="Name eingeben..." autocomplete="off" minlength="2" maxmlength="255" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">E-Mail Adresse</label>
                        <input id="email" name="email" type="email" class="form-control" placeholder="E-Mail Adresse eingeben..." autocomplete="off" minlength="4" maxmlength="255" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Passwort</label>
                        <div class="input-group input-group-flat">
                            <input id="password" name="password" type="password" class="form-control" placeholder="Passwort eingeben..." autocomplete="off" minlength="8" maxmlength="255" required>
                            <span class="input-group-text">
                  <a href="#" class="link-secondary" title="" data-bs-toggle="tooltip" data-bs-original-title="Passwort anzeigen/ausblenden" onclick="showPassword()">
                      <img id="password_eye" src="<?php echo site_url('static/icons/eye.svg'); ?>" width="24" height="24" alt="eye" class="icon">
                  </a>
                </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Passwort wiederholen</label>
                        <div class="input-group input-group-flat">
                            <input id="confirmpassword" name="confirmpassword" type="password" class="form-control" placeholder="Passwort nochmal eingeben..." autocomplete="off" minlength="8" maxmlength="255" required>
                            <span class="input-group-text">
                  <a href="#" class="link-secondary" title="" data-bs-toggle="tooltip" data-bs-original-title="Passwort anzeigen/ausblenden" onclick="showPassword()">
                      <img id="confirmpassword_eye" src="<?php echo site_url('static/icons/eye.svg'); ?>" width="24" height="24" alt="eye" class="icon">
                  </a>
                </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-check">
                            <input id="terms" name="terms" type="checkbox" class="form-check-input" required>
                            <span class="form-check-label">Stimmen Sie den <a href="<?php echo site_url('terms'); ?>" tabindex="-1">Nutzungsbedingungen</a> zu.</span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary w-100">Erstellen</button>
                    </div>
                </div>
            </form>
            <div class="text-center text-muted mt-3">
                Haben Sie bereits ein Konto? Zur <a href="<?php echo site_url('login'); ?>" tabindex="-1">Anmeldung</a>!
            </div>
        </div>
    </div>

    <script>
        function showPassword() {
            var password = document.getElementById("password");
            var password_eye = document.getElementById("password_eye");
            var confirmpassword = document.getElementById("confirmpassword");
            var confirmpassword_eye = document.getElementById("confirmpassword_eye");

            if (password.type === "password") {
                password.type = "text";
                password_eye.src = "./static/icons/eye-off.svg";
                confirmpassword.type = "text";
                confirmpassword_eye.src = "./static/icons/eye-off.svg";
            } else {
                password.type = "password";
                password_eye.src = "./static/icons/eye.svg";
                confirmpassword.type = "password";
                confirmpassword_eye.src = "./static/icons/eye.svg";
            }
        }
    </script>