<?php
$session = session();
helper('cookie');
helper('login_helper');

// Login Check
CheckLoggedIn();

$router = service('router');
$controller = explode("\\", $router->controllerName());
$method = $router->methodName();
?>
<body class="antialiased theme-dark">
<div class="wrapper">
    <header class="navbar navbar-expand-md navbar-light d-print-none">
        <div class="container-xl">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu">
                <span class="navbar-toggler-icon"></span>
            </button>
            <h1 class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
                <a href="<?php echo site_url('/'); ?>">
                    <img src="<?php echo site_url('/static/logo.svg'); ?>" class="navbar-brand-image" width="110" height="32" alt="Streamerliste" viewBox="0 0 110 32" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                         stroke-linejoin="round">
                </a>
            </h1>
            <?php
            if (ENVIRONMENT == "development") {
                echo "[" . ucfirst(ENVIRONMENT) . "]";
            }

            // Angemeldet
            if ($session->get('auth_LoggedIn')) {
                ?>
                <div class="navbar-nav flex-row order-md-last">
                    <div class="nav-item dropdown">
                        <?php
                        if ($session->get('auth_LoggedIn')) {
                            ?>
                            <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
                                <div class="d-none d-xl-block ps-2">
                                    <div><?php echo $session->get('auth_name'); ?></div>
                                    <div class="mt-1 small text-muted">
                                        <?php
                                        if ($session->get('auth_permissions') >= 10) {
                                            echo "Administrator";
                                        } else {
                                            echo "Benutzer";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </a>
                            <?php
                        }
                        ?>
                        <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                            <?php
                            if ($session->get('auth_LoggedIn') && $session->get('auth_permissions') >= 10) {
                                ?>
                                <a href="#" class="dropdown-item">
                                    <img src="<?php echo site_url('static/icons/tool.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Administration" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                    Administration
                                </a>
                                <?php
                            }
                            ?>
                            <a href="#" class="dropdown-item">
                                <img src="<?php echo site_url('static/icons/settings.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Settings" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                     stroke-linecap="round" stroke-linejoin="round">
                                Einstellungen
                            <div class="dropdown-divider"></div>
                            <a href="<?php echo site_url('logout'); ?>" class="dropdown-item">
                                <img src="<?php echo site_url('static/icons/logout.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Logout" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                     stroke-linecap="round" stroke-linejoin="round">
                                Ausloggen
                            </a>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                // Nicht Angemeldet!
                ?>
                <div class="nav-item d-none d-md-flex me-3">
                    <div class="btn-list">
                        <a href="<?php echo site_url('login'); ?>" class="btn btn-primary" rel="noreferrer">
                            <img src="<?php echo site_url('static/icons/login.svg'); ?>" width="24" height="24" alt="login" class="icon">
                            Anmelden
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </header>
    <div class="navbar-expand-md">
        <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="navbar navbar-light">
                <div class="container-xl">
                    <ul class="navbar-nav">
                        <li class="nav-item <?php if (isset($controller[3]) && $controller[3] == "Home") {
                            echo "active";
                        } ?>">
                            <a class="nav-link" href="<?php echo site_url('/'); ?>">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                        <img src="<?php echo site_url('static/icons/home.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Home" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round">
                    </span>
                                <span class="nav-link-title">
                      Home
                    </span>
                            </a>
                        </li>
                        <li class="nav-item <?php if (isset($controller[3]) && ($controller[3] == "Numberplates" && $method == "all")) {
                            echo "active";
                        } ?>">
                            <a class="nav-link" href="<?php echo site_url('numberplates'); ?>">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                        <img src="<?php echo site_url('static/icons/list.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Home" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round">
                    </span>
                                <span class="nav-link-title">
                      Alle Kennzeichen
                    </span>
                            </a>
                        </li>
                        <li class="nav-item <?php if (isset($controller[3]) && ($controller[3] == "Numberplates" && $method == "converter")) {
                            echo "active";
                        } ?>">
                            <a class="nav-link" href="<?php echo site_url('converter'); ?>">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                        <img src="<?php echo site_url('static/icons/exchange.svg'); ?>" class="icon dropdown-item-icon" width="24" height="24" alt="Alle Kennzeichen" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round">
                    </span>
                                <span class="nav-link-title">
                      Umwandlung
                    </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>