<br/><br/><br/><br/>
</div>
</div>
<?php
$router = service('router');
$controller = $router->controllerName();

if (get_cookie('cookies') == null && !in_array($controller, array("\App\Controllers\Privacy", "\App\Controllers\Imprint", "\App\Controllers\Contact"))) {
    ?>
    <div class="modal modal-blur fade" id="modal-report" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cookie-Einstellungen</h5>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <h3>Diese Webseite verwendet Cookies</h3>
                        <div class="text">Wir verwenden Cookies, um Ihnen ein optimales Webseiten-Erlebnis zu bieten. Dazu zählen Cookies, die für den Betrieb der Seite notwendig sind, sowie solche, die lediglich zu anonymen Statistikzwecken,
                            für Komforteinstellungen oder zur Anzeige personalisierter Inhalte genutzt werden. Sie können selbst entscheiden, welche Kategorie Sie zulassen möchten. Bitte beachten Sie, dass auf Basis Ihrer Einstellungen womöglich
                            nicht mehr alle Funktionalitäten der Seite zur Verfügung stehen. Weitere Informationen finden Sie in unserer
                            <a href="<?php echo site_url('privacy'); ?>" class="link">Datenschutzerklärung</a>.
                        </div>
                    </div>
                    <label class="form-label">Kategorien</label>
                    <div class="form-selectgroup-boxes row mb-3">
                        <div class="col-lg-4">
                            <label class="form-selectgroup-item">
                                <input type="checkbox" name="cookieCategory" value="1" class="form-selectgroup-input" checked disabled>
                                <span class="form-selectgroup-label d-flex align-items-center p-3">
                                    <span class="me-3">
                                        <span class="form-selectgroup-check"></span>
                                    </span>
                                    <span class="form-selectgroup-label-content">
                                        <span class="form-selectgroup-title strong mb-1">Notwendig</span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <div class="col-lg-4">
                            <label class="form-selectgroup-item">
                                <input type="checkbox" name="cookieCategory" value="2" class="form-selectgroup-input">
                                <span class="form-selectgroup-label d-flex align-items-center p-3">
                                    <span class="me-3">
                                        <span class="form-selectgroup-check"></span>
                                    </span>
                                    <span class="form-selectgroup-label-content">
                                        <span class="form-selectgroup-title strong mb-1">Statistik</span>
                                    </span>
                                </span>
                            </label>
                        </div>
                        <div class="col-lg-4">
                            <label class="form-selectgroup-item">
                                <input type="checkbox" name="cookieCategory" value="3" class="form-selectgroup-input">
                                <span class="form-selectgroup-label d-flex align-items-center p-3">
                                    <span class="me-4">
                                        <span class="form-selectgroup-check"></span>
                                    </span>
                                    <span class="form-selectgroup-label-content">
                                        <span class="form-selectgroup-title strong mb-1">Komfort</span>
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo site_url('imprint'); ?>" class="btn btn-link link-secondary">
                        Impressum
                    </a>
                    <a href="<?php echo site_url('privacy'); ?>" class="btn btn-link link-secondary">
                        Datenschutzerklärung
                    </a>
                    <div class="col-auto ms-auto d-print-none">
                        <div class="btn-group w-100">
                            <button type="button" class="btn btn-yellow" onclick="selectAll()"><img src="<?php echo site_url('static/icons/checkbox.svg'); ?>" width="24" height="24" alt="Alle" class="icon">Alle</button>
                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="setCookieSettings()"><img src="<?php echo site_url('static/icons/check.svg'); ?>" width="24" height="24" alt="Bestätigen" class="icon">Bestätigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="btn btn-white" id="cookie-banner" data-bs-toggle="modal" data-bs-target="#modal-report" hidden>Open Cookie Banner!</a>
    <script src="<?php echo base_url('dist/js/cookie-banner.js'); ?>"></script>
    <?php
}
?>
<footer class="footer footer-transparent d-print-none">
    <div class="container">
        <div class="row text-center align-items-center flex-row-reverse">
            <div class="col-lg-auto ms-lg-auto">
                <ul class="list-inline list-inline-dots mb-0">
                    <li class="list-inline-item"><a href="<?php echo site_url('imprint'); ?>" class="link-secondary">Impressum</a></li>
                    <li class="list-inline-item"><a href="<?php echo site_url('privacy'); ?>" class="link-secondary">Datenschutzerklärung</a></li>
                    <li class="list-inline-item"><a href="<?php echo site_url('terms'); ?>" class="link-secondary">Nutzungsbedingungen</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                Copyright © <?php echo date("Y"); ?>
                <a href="https://kevinjonas.de/" class="link-secondary">Kevin,</a>
				Enrico und Gregor.
                All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- Libs JS -->
<!--<script src="<?php //echo base_url('dist/libs/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>-->
<!--<script src="<?php //echo base_url('dist/libs/jquery/dist/jquery.slim.min.js'); ?>"></script>-->
<!-- Tabler Core -->
<script src="<?php echo base_url('dist/js/tabler.min.js'); ?>"></script>
</body>
</html>