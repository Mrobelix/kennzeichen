<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="description" content="Kennzeichenliste"/>
    <meta name="keywords" content="Berufsschule, Schule, Abschlussprojekt, Projekt, Kennzeichen, Kennzeichenliste"/>
    <meta name="msapplication-TileColor" content="#206bc4"/>
    <meta name="theme-color" content="#206bc4"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="MobileOptimized" content="320"/>
    <meta name="robots" content="noindex,nofollow,noarchive"/>

    <?php
    if (is_cli() == false) {
        if (!in_array($_SERVER['REQUEST_URI'], array("/noscript", "/imprint", "/privacy", "/contact"))) {
            ?>
            <noscript>
                <meta http-equiv="refresh" content="0; URL=<?php echo site_url('noscript'); ?>">
            </noscript>
            <?php
        }
    }

    if (isset($bc_title)) {
        echo "<title>Kennzeichen: " . $bc_title . "</title>";
    } else {
        echo "<title>Kennzeichen</title>";
    }
    ?>

    <link rel="icon" href="<?php echo base_url('static/favicon.ico'); ?>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url('static/favicon.ico'); ?>" type="image/x-icon"/>
    <!-- Tabler Core -->
    <link href="<?php echo base_url('dist/css/tabler.min.css') . '?time=' . time(); ?>" rel="stylesheet"/>
    <!-- Tabler Plugins -->
    <link href="<?php echo base_url('dist/css/tabler-vendors.min.css') . '?time=' . time(); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('dist/css/demo.min.css') . '?time=' . time(); ?>" rel="stylesheet"/>
    <!-- VueJS -->
    <?php
    if (ENVIRONMENT == "development") {
        ?>
        <script src="<?php echo base_url('/dist/libs/vuejs/dist/js/vue.js'); ?>"></script>
        <?php
    } else {
        ?>
        <script src="<?php echo base_url('/dist/libs/vuejs/dist/js/vue.prod.js'); ?>"></script>
        <?php
    }
    ?>
</head>