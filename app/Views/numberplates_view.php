<?php
$session = session();
?>

<div class="page-wrapper">
    <div id="numberplates">
        <numberplate-card pretitle="<?php if (isset($bc_pretitle)) {
            echo $bc_pretitle;
        } else {
            echo "none";
        } ?>" title="<?php if (isset($bc_title)) {
            echo $bc_title;
        } else {
            echo "none";
        } ?>" permissions="<?php if ($session->get('auth_permissions')) {
            echo $session->get('auth_permissions');
        } else {
            echo "none";
        } ?>" alert_type="<?php if ($session->get('alert_type') != 0) {
            echo $session->get('alert_type');
        } else {
            echo "none";
        } ?>" alert_title="<?php if ($session->get('alert_title') != 0) {
            echo $session->get('alert_title');
        } else {
            echo "none";
        } ?>" alert_text="<?php if ($session->get('alert_text') != 0) {
            echo $session->get('alert_text');
        } else {
            echo "none";
        } ?>">

            <!-- Body Head -->
            <div class="container-xl">
                <div class="page-header d-print-none">
                    <div class="row align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <div class="page-pretitle">
                                <?php echo $bc_pretitle; ?>
                            </div>
                            <!-- Page title -->
                            <h2 class="page-title">
                                <?php echo $bc_title; ?>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Loading -->
            <div class="page-body">
                <div class="container-xl">
                    <div class="col-sm-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Seite wird geladen<span class="animated-dots"></span></h3>
                            </div>
                            <div class="card-body">
                                <p>Die Kennzeichenliste wird gerade geladen!</p>
                                <div class="spinner-border" role="status"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </numberplate-card>
    </div>
</div>

<!-- Import-Modal -->
<?php
if ($session->get('auth_permissions') >= 10) {
?>
<div class="modal modal-blur fade" id="modal-import" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <?= form_open_multipart('import'); ?>
                <div class="modal-header">
                    <h5 class="modal-title">Kennzeichenliste importieren</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        Hier können Sie ihre Kennzeichenliste importieren!<br/>
                        Wählen Sie die Datei aus (CSV, JSON oder XML)
                    </div>
                    <div class="mb-3">
                        <div class="form-label">Datei</div>
                        <input type="file" id="file" name="file" class="form-control" accept=".csv,.json,.xml">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-white" data-bs-dismiss="modal">
                        Zurück
                    </a>
                    <button type="submit" value="Upload" class="btn btn-primary ms-auto">
                        <img src="/static/icons/file-import.svg" width="24" height="24" alt="Importieren" class="icon">
                        Importieren
                    </button>
                </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>
<?php
}
?>

<script src="<?php echo base_url('dist/js/vue/numberplates.js'); ?>"></script>

<?php
if ($session->get('alert_type')) {
    $session->remove('alert_type');
    $session->remove('alert_title');
    $session->remove('alert_text');
}
?>