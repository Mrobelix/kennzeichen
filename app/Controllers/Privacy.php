<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf der Datenschutzerklärung ausgeführt wird.
 *
 * @author Kevin Jonas
 */

class Privacy extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    public function index()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Übersicht',
            'bc_title' => 'Datenschutzerklärung'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('privacy_view');
        echo view('includes/footer_view');
    }
}
