<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf des Impressums ausgeführt wird.
 *
 * @author Kevin Jonas
 */
 
class Imprint extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    public function index()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Übersicht',
            'bc_title' => 'Impressum'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('imprint_view');
        echo view('includes/footer_view');
    }
}
