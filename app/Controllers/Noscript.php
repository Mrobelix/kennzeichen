<?php namespace App\Controllers;

/**
 * Description: Controller, der Aufgerufen wird, wenn kein JavaScript aktiviert ist.
 *
 * @author Kevin Jonas
 */

class Noscript extends BaseController
{
	public function index()
	{
        $breadcrumb = array(
            'bc_pretitle' => 'Fehlermeldung',
            'bc_title' => 'Noscript'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('noscript_view');
        echo view('includes/footer_view');
	}
}
