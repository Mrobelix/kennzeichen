<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf aller Kennzeichen ausgeführt wird.
 *
 * @author Kevin Jonas
 */

class Home extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

	public function index()
	{
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Startseite'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('home_view');
		
        echo view('includes/footer_view');
	}
}
