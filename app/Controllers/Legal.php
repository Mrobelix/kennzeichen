<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf des Impressums ausgeführt wird.
 *
 * @author Kevin Jonas
 */

class Legal extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    public function imprint()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Übersicht',
            'bc_title' => 'Impressum'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('legal/imprint_view');
        echo view('includes/footer_view');
    }

    public function privacy()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Übersicht',
            'bc_title' => 'Datenschutzerklärung'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('legal/privacy_view');
        echo view('includes/footer_view');
    }

    public function terms()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Übersicht',
            'bc_title' => 'Nutzungsbedingungen'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('legal/terms_view');
        echo view('includes/footer_view');
    }
}
