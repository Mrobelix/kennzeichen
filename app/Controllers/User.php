<?php namespace App\Controllers;

use App\Models\UserModel;

/**
 * Description: Controller, der beim Aufruf aller Kennzeichen ausgeführt wird.
 *
 * @author Kevin Jonas
 */

class User extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    // Login
    public function login()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Anmelden'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('user/login_view');

        echo view('includes/footer_view');
    }

    public function loginaction()
    {
        $session = session();
        $userModel = new UserModel();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $data = $userModel->where('email', $email)->first();

        if($data){
            $authenticatePassword = password_verify($password, $data['password']);
            if($authenticatePassword){
                // Generate random 32 Character Token
                $token = substr(str_shuffle("!?#0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 32);

                // Set Session Data
                $ses_data = [
                    'auth_id' => $data['id'],
                    'auth_name' => $data['name'],
                    'auth_email' => $data['email'],
                    'auth_permissions' => $data['permissions'],
                    'auth_token' => $token,
                    'auth_LoggedIn' => '1'
                ];

                $session->set($ses_data);

                // Update Database Data
                $db_data = [
                    'token' => $token,
                    'last_login' => date("Y-m-d H:i:s")
                ];

                $userModel->update($data['id'], $db_data);

                // Set Alert Session
                $session->set('alert_type', 'alert-success');
                $session->set('alert_title', 'Sie wurden erfolgreich Angemeldet!');
                $session->set('alert_text', 'Sie können nun von allen Vorteilen der Kennzeichenliste profitieren!');

                // Redirect to Home
                return redirect()->to(site_url('/'));
            }else{
                // Set Alert Session
                $session->set('alert_type', 'alert-danger');
                $session->set('alert_title', 'Sie konnten nicht Angemeldet werden!');
                $session->set('alert_text', 'Das Passwort ist nicht korrekt!');

                // Redirect back to login
                return redirect()->to(site_url('login'));
            }
        }else{
            // Set Alert Session
            $session->set('alert_type', 'alert-danger');
            $session->set('alert_title', 'Sie konnten nicht Angemeldet werden!');
            $session->set('alert_text', 'Es existiert keine Konto mit dieser E-Mail Adresse');

            // Redirect back to login
            return redirect()->to(site_url('login'));
        }
    }

    // Logout
    public function logout()
    {
        $session = session();
        $session->destroy();

        return redirect()->to(site_url('/'));
    }

    // Register
    public function register()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Registrierung'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('user/register_view');

        echo view('includes/footer_view');
    }

    public function registeraction()
    {
        // Start the Session
        $session = session();

        // Validation Rules
        $rules = [
            'name'              => 'required|min_length[2]|max_length[255]',
            'email'             => 'required|min_length[4]|max_length[255]|valid_email|is_unique[users.email]',
            'password'          => 'required|min_length[8]|max_length[255]',
            'confirmpassword'   => 'required|matches[password]',
            'terms'             => 'required'
        ];

        // Validation Check
        if($this->validate($rules)){
            $userModel = new UserModel();

            // Set Data for the Database
            $data = [
                'name'     => $this->request->getVar('name'),
                'email'    => $this->request->getVar('email'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT)
            ];

            // Add the User to the Database
            $userModel->save($data);

            // Set Alert Session
            $session->set('alert_type', 'alert-success');
            $session->set('alert_title', 'Konto wurde erfolgreich erstellt!');
            $session->set('alert_text', 'Sie können sich nun Anmelden!');

            // Redirect to login
            return redirect()->to(site_url('login'));
        }else{
            // Set Alert Session
            $session->set('alert_type', 'alert-danger');
            $session->set('alert_title', 'Konto konnte nicht erstellt werden!');
            $session->set('alert_text', 'Bitte versuchen Sie es erneut!');
            $session->set('alert_errors', $this->validator->getErrors());

            // Redirect back to register
            return redirect()->to(site_url('register'));
        }
    }

    // Forgotpassword
    public function forgotpassword()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Passwort vergessen'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('user/forgotpassword_view');

        echo view('includes/footer_view');
    }
}
