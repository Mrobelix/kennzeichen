<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf der API ausgeführt wird.
 *
 * @author Kevin Jonas
 */

use App\Models\NumberplateModel;

class API extends BaseController
{
	public function fetchShortcuts()
	{
		$this->response->setContentType('Content-Type: application/json');

		$db = db_connect();
		$NumberplateModel = new NumberplateModel($db);
				
		$Shortcuts = $NumberplateModel->getShortcuts();
		
		foreach ($Shortcuts as $Shortcut) {
			$result[$Shortcut->shortcut] = $Shortcut->shortcut;
		}
		
		return json_encode($result);
	}
	
	public function fetchDistricts()
	{
		$this->response->setContentType('Content-Type: application/json');

		$db = db_connect();
		$NumberplateModel = new NumberplateModel($db);
				
		$Districts = $NumberplateModel->getDistricts();
		
		foreach ($Districts as $District) {
			$result[$District->district] = $District->district;
		}
		
		return json_encode($result);
	}

    public function fetchStates()
    {
        $this->response->setContentType('Content-Type: application/json');

        $db = db_connect();
        $NumberplateModel = new NumberplateModel($db);

        $States = $NumberplateModel->getStates();

        foreach ($States as $State) {
            $result[$State->shortcut] = Array(
                "shortcut" => $State->shortcut,
                "state" => $State->state
            );
        }

        return json_encode($result);
    }
	
	public function fetchNumberplates()
	{
		$this->response->setContentType('Content-Type: application/json');
		
		$db = db_connect();
		$NumberplateModel = new NumberplateModel($db);
				
		$Numberplates = $NumberplateModel->getNumberplates();
		
		return json_encode($Numberplates);
	}
}
