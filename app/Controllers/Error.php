<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf eines Fehlers ausgeführt wird.
 *
 * @author Kevin Jonas
 */
 
class Error extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    public function index($errorcode = 404)
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Fehlermeldung',
            'bc_title' => $errorcode
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('error_view');
        echo view('includes/footer_view');
    }

    public function noscript()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Fehlermeldung',
            'bc_title' => 'Noscript'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('noscript_view');
        echo view('includes/footer_view');
    }
}
