<?php namespace App\Controllers;

/**
 * Description: Controller, der beim Aufruf der Umwandung ausgeführt wird.
 *
 * @author Kevin Jonas
 */

class Converter extends BaseController
{
	public function index()
	{
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Umwandlung'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('converter_view');
		
        echo view('includes/footer_view');
	}
}
