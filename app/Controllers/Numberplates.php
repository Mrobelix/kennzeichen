<?php namespace App\Controllers;

use App\Models\NumberplateModel;

/**
 * Description: Controller, der beim Aufruf aller Kennzeichen ausgeführt wird.
 *
 * @author Kevin Jonas
 */
class Numberplates extends BaseController
{
    public function __construct()
    {
        session()->start();
        helper('cookie');
    }

    // Numberplates
    public function all()
    {
		helper(['form', 'url']);
		
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Alle Kennzeichen'
        );

        if (get_cookie('layout') == null) {
            set_cookie('layout', 'list', 31536000, '', '/');
        }

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('numberplates_view');

        echo view('includes/footer_view');
    }

    public function edit()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {
            // Validation-Rules
            $validation = $this->validate([
                'shortcut' => 'required',
                'origin' => 'required',
                'district' => 'required',
                'state' => 'required'
            ]);

            if ($validation) {
                // Database connection and creation of the model
                $db = db_connect();
                $NumberplateModel = new NumberplateModel($db);

                // POST Variables
                $shortcut = $this->request->getPostGet('shortcut');
                $origin = $this->request->getPostGet('origin');
                $district = $this->request->getPostGet('district');
                $state = $this->request->getPostGet('state');

                // Check if the numberplate exist
                if (!$NumberplateModel->existNumberplate($shortcut)) {
                    // Numberplate does not exist
                    $session->set("alert_type", "success");
                    $session->set("alert_title", "Kennzeichen wurde bearbeitet!");
                    $session->set("alert_text", "Das Kennzeichen (" . $shortcut . ") wurde erfolgreich bearbeitet!");

                    // Update Numberplate
                    $NumberplateModel->updateNumberplate($shortcut, $origin, $district, $state);
                } else {
                    // Numberplate exists
                    $session->set("alert_type", "danger");
                    $session->set("alert_title", "Kennzeichen konnte nicht bearbeitet werden!");
                    $session->set("alert_text", "Das Kennzeichen konnte nicht bearbeitet werden, da es nicht existiert!");
                }

            } else {
                // Fields empty
                $session->set("alert_type", "danger");
                $session->set("alert_title", "Kennzeichen konnte nicht bearbeitet werden!");
                $session->set("alert_text", "Die Felder dürfen nicht leer sein!");
            }

        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichen konnte nicht bearbeitet werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, ein Kennzeichen zu bearbeiten!");
        }

        // Redirect to Numberplate View
        return redirect('numberplates');
    }

    public function add()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {
            // Validation-Rules
            $validation = $this->validate([
                'shortcut' => 'required',
                'origin' => 'required',
                'district' => 'required',
                'state' => 'required'
            ]);

            if ($validation) {
                // Database connection and creation of the model
                $db = db_connect();
                $NumberplateModel = new NumberplateModel($db);

                // POST Variables
                $shortcut = $this->request->getPostGet('shortcut');
                $origin = $this->request->getPostGet('origin');
                $district = $this->request->getPostGet('district');
                $state = $this->request->getPostGet('state');

                // Check if the numberplate exist
                if ($NumberplateModel->existNumberplate($shortcut)) {
                    // Numberplate does not exist
                    $session->set("alert_type", "success");
                    $session->set("alert_title", "Kennzeichen wurde erstellt!");
                    $session->set("alert_text", "Das Kennzeichen (" . $shortcut . ") wurde erfolgreich erstellt!");

                    // Add Numberplate
                    $NumberplateModel->addNumberplate($shortcut, $origin, $district, $state);
                } else {
                    // Numberplate exists
                    $session->set("alert_type", "danger");
                    $session->set("alert_title", "Kennzeichen konnte nicht erstellt werden!");
                    $session->set("alert_text", "Das Kennzeichen (" . $shortcut . ") existiert bereits!");
                }

            } else {
                // Fields empty
                $session->set("alert_type", "danger");
                $session->set("alert_title", "Kennzeichen konnte nicht erstellt werden!");
                $session->set("alert_text", "Die Felder dürfen nicht leer sein!");
            }
        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichen konnte nicht erstellt werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, ein Kennzeichen zu erstellen!");
        }

        // Redirect to Numberplate View
        return redirect('numberplates');
    }

    public function delete()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {
            // Validation-Rules
            $validation = $this->validate([
                'shortcut' => 'required'
            ]);

            if ($validation) {
                // Database connection and creation of the model
                $db = db_connect();
                $NumberplateModel = new NumberplateModel($db);

                // POST Variables
                $shortcut = $this->request->getPostGet('shortcut');

                // Check if the numberplate exist
                if ($NumberplateModel->existNumberplate($shortcut)) {
                    // Numberplate does not exist
                    $session->set("alert_type", "danger");
                    $session->set("alert_title", "Kennzeichen konnte nicht gelöscht werden!");
                    $session->set("alert_text", "Das Kennzeichen konnte nicht gelöscht werden, da es nicht existiert!");
                } else {
                    // Numberplate exists
                    $session->set("alert_type", "success");
                    $session->set("alert_title", "Kennzeichen erfolgreich gelöscht!");
                    $session->set("alert_text", "Das Kennzeichen (" . $shortcut . ") wurde erfolgreich gelöscht!");

                    // Delete Numberplate
                    $NumberplateModel->deleteNumberplate($shortcut);
                }

            } else {
                // Fields empty
                $session->set("alert_type", "danger");
                $session->set("alert_title", "Kennzeichen konnte nicht gelöscht werden!");
                $session->set("alert_text", "Die Felder dürfen nicht leer sein!");
            }
        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichen konnte nicht gelöscht werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, ein Kennzeichen zu löschen!");
        }

        // Redirect to Numberplate View
        return redirect('numberplates');
    }

    // Numberplate Export
    public function exportXML()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {

            // Define Header
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=Numberplate_Database_" . date("Y-m-d_H:i:s") . ".xml");
            header("Content-Type: application/xml; ");

            $file = fopen('php://output', 'w');

            // Loading Export Helper
            helper('export_helper');

            // Database connection and creation of the model
            $db = db_connect();
            $NumberplateModel = new NumberplateModel($db);

            // Get all Numberplates
            $Numberplates = json_decode(json_encode($NumberplateModel->getNumberplates()), true);

            // Array to XML
            $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
            array_to_xml($Numberplates, $xml_data);
            $xml_data->asXML('php://output');

            fclose($file);

        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichenlist kann nicht exportiert werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, die Kennzeichenliste zu exportieren!");

            // Redirect to Numberplate View
            return redirect('numberplates');
        }
    }

    public function exportCSV()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {

            // Define Header
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=Numberplate_Database_" . date("Y-m-d_H:i:s") . ".csv");
            header("Content-Type: application/csv; ");

            $file = fopen('php://output', 'w');

            // Loading Export Helper
            helper('export_helper');

            // Database connection and creation of the model
            $db = db_connect();
            $NumberplateModel = new NumberplateModel($db);

            // Get all Numberplates
            $Numberplates = $NumberplateModel->getNumberplates();

            // Create CSV-Header
            fputcsv($file, array("shortcut", "origin", "district", "state"));

            // Put Numberplates in SCV-File
            foreach ($Numberplates as $Numberplate) {
                fputcsv($file, array($Numberplate->shortcut, $Numberplate->origin, $Numberplate->district, $Numberplate->state));
            }

            fclose($file);

        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichenlist kann nicht exportiert werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, die Kennzeichenliste zu exportieren!");

            // Redirect to Numberplate View
            return redirect('numberplates');
        }
    }

    public function exportJSON()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {

            // Define Header
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=Numberplate_Database_" . date("Y-m-d_H:i:s") . ".json");
            header("Content-Type: application/json; ");

            $file = fopen('php://output', 'w');

            // Loading Export Helper
            helper('export_helper');

            // Database connection and creation of the model
            $db = db_connect();
            $NumberplateModel = new NumberplateModel($db);

            // Get all Numberplates
            $Numberplates = $NumberplateModel->getNumberplates();

            // Array to JSON
            echo json_encode($Numberplates);

            fclose($file);

        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichenlist kann nicht exportiert werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, die Kennzeichenliste zu exportieren!");

            // Redirect to Numberplate View
            return redirect('numberplates');
        }
    }

    // Numberplate Import
    public function import()
    {
        $session = session();

        // The user must be logged in and an administrator
        if ($session->get('auth_LoggedIn') == "1" && $session->get('auth_permissions') >= 10) {

            // Loading Form and URL Helper
            helper(['form', 'url']);

            // Validation-Rules
            $input = $this->validate([
                'file' => 'uploaded[file]'
            ]);

            // File exist and is Valid
            if ($input) {
                if($file = $this->request->getFile('file')) {
                    if ($file->isValid() && !$file->hasMoved()) {

                        // Database connection and creation of the model
                        $db = db_connect();
                        $NumberplateModel = new NumberplateModel($db);

                        // Variables
                        $extension = $file->getExtension();
                        $filearray = array();
                        $i = 0;
                        $success = 0;

                        // Upload File
                        $file->move('.', 'Numberplates.'.$extension);

                        // XML-File
                        if ($extension == "csv") {
                            // Read the CSV-File and Convert to Array
                            $csv = fopen($file->getName(),"r");

                            while (($filedata = fgetcsv($csv, 1000, ",")) !== FALSE) {
                                $num = count($filedata);
                                if($i > 0 && $num == 4){
                                    $filearray[$i]['shortcut'] = $filedata[0];
                                    $filearray[$i]['origin'] = $filedata[1];
                                    $filearray[$i]['district'] = $filedata[2];
                                    $filearray[$i]['state'] = ucfirst($filedata[3]);
                                }
                                $i++;
                            }

                            fclose($csv);
                        // XML-File
                        } else if ($extension == "xml") {
                            // Read the XML-File and Convert to Array
                            $xml = simplexml_load_file($file->getName());

                            foreach ($xml->children() as $children) {
                                $i++;
                                $filearray[$i]['shortcut'] = $children->shortcut->__toString();
                                $filearray[$i]['origin'] = $children->origin->__toString();
                                $filearray[$i]['district'] = $children->district->__toString();
                                $filearray[$i]['state'] = ucfirst($children->state->__toString());
                            }

                        // JSON-File
                        } else if ($extension == "json") {
                            // Read the JSON-File and Convert to Array
                            echo "JSON";

                            $json = json_decode(file_get_contents($file->getName()));

                            foreach ($json as $object) {
                                $i++;
                                $filearray[$i]['shortcut'] = $object->shortcut;
                                $filearray[$i]['origin'] = $object->origin;
                                $filearray[$i]['district'] = $object->district;
                                $filearray[$i]['state'] = ucfirst($object->state);
                            }

                        } else {
                            // Validation failed
                            $session->set("alert_type", "danger");
                            $session->set("alert_title", "Kennzeichenlist kann nicht importiert werden!");
                            $session->set("alert_text", "Es dürfen nur CSV, JSON oder XML Dateien verwendet werden!");

                            return redirect('numberplates');
                        }

                        // Adding the Numberplates that do not exist
                        foreach ($filearray as $numberplate) {
                            if ($NumberplateModel->existNumberplate($numberplate['shortcut'])) {
                                $NumberplateModel->addNumberplate($numberplate['shortcut'], $numberplate['origin'], $numberplate['district'], $NumberplateModel->getShortState($numberplate['state']));
                                $success++;
                            }
                        }

                        // Delete uploaded File
                        unlink($file->getName());

                        if ($success == 0) {
                            // All Numberplates are already present
                            $session->set("alert_type", "danger");
                            $session->set("alert_title", "Kennzeichenlist wird nicht importiert!");
                            $session->set("alert_text", "Es wurde kein neues Kennzeichen in ihrer Datei gefunden!");
                        } else {
                            // Numberplates that do not exist yet are imported successfully
                            $session->set("alert_type", "success");
                            $session->set("alert_title", "Kennzeichenlist wurde importiert!");
                            $session->set("alert_text", "Es wurde ".$success." von ".count($filearray)." neuen Kennzeichen aus ihrer Datei importiert!");
                        }
                    }
                }
            } else {
                // Validation failed
                $session->set("alert_type", "danger");
                $session->set("alert_title", "Kennzeichenlist kann nicht importiert werden!");
                $session->set("alert_text", "Es ist ein Fehler aufgetreten, bitte nochmal versuchen!");
            }

        } else {
            // Not logged in or not an administrator
            $session->set("alert_type", "danger");
            $session->set("alert_title", "Kennzeichenlist kann nicht importiert werden!");
            $session->set("alert_text", "Sie haben keine Berechtigung, eine Kennzeichenliste zu importieren!");
        }

        // Redirect to Numberplate View
        return redirect('numberplates');
    }

    // Converter
    public function converter()
    {
        $breadcrumb = array(
            'bc_pretitle' => 'Kennzeichen',
            'bc_title' => 'Umwandlung'
        );

        echo view('includes/head_view', $breadcrumb);
        echo view('includes/header_view');

        echo view('converter_view');

        echo view('includes/footer_view');
    }
}
