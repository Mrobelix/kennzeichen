<?php

use App\Models\UserModel;

function CheckLoggedIn()
{
    $session = session();
    $userModel = new UserModel();

    if ($session->get('auth_LoggedIn')) {

        $router = service('router');

        //echo "<pre>";
        //print_r($router->controllerName());
        //echo "</pre>";

        $controller = explode("\\", $router->controllerName())[3];
        $method = $router->methodName();

        $data = $userModel->where('id', $session->get('auth_id'))->first();

        // Check the Token
        if ($session->get('auth_token') == $data['token']) {
            // Set Session Data
            $ses_data = [
                'auth_id' => $data['id'],
                'auth_name' => $data['name'],
                'auth_email' => $data['email'],
                'auth_permissions' => $data['permissions'],
                'auth_LoggedIn' => '1'
            ];

            $session->set($ses_data);

            // Redirect if LoggedIn
            if ($session->get('auth_LoggedIn') && $controller == "User" && $method != "logout") {
                echo '<meta http-equiv="refresh" content="0; URL='.site_url('/').'">';
            }

            return true;
        }

        // Token mismatch = Logout
        $session->destroy();
        return false;
    }
    return false;
}
