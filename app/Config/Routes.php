<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override('App\Controllers\Error::index');
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// General (Home, NoScript, Errors)
$routes->get('/', 'Home::index');
$routes->get('noscript', 'Error::noscript');
$routes->get('error/(:any)', 'Error::index/$1');

// Legal
$routes->get('imprint', 'Legal::imprint');
$routes->get('privacy', 'Legal::privacy');
$routes->get('terms', 'Legal::terms');

// Numberplates
$routes->get('numberplates', 'Numberplates::all');
$routes->get('converter', 'Numberplates::converter');

// Numberplate Actions
$routes->post('editnumberplate', 'Numberplates::edit');
$routes->post('addnumberplate', 'Numberplates::add');
$routes->post('deletenumberplate', 'Numberplates::delete');

// Export/Import
$routes->get('exportXML', 'Numberplates::exportXML');
$routes->get('exportCSV', 'Numberplates::exportCSV');
$routes->get('exportJSON', 'Numberplates::exportJSON');
$routes->post('import', 'Numberplates::import');

// User
$routes->get('login', 'User::login');
$routes->get('logout', 'User::logout');
$routes->get('register', 'User::register');
$routes->get('forgotpassword', 'User::forgotpassword');

// User Actions
$routes->post('loginaction', 'User::loginaction');
$routes->post('registeraction', 'User::registeraction');
$routes->post('forgotpasswordaction', 'User::forgotpasswordaction');

// API
$routes->group('api', function ($routes) {
    $routes->get('fetchShortcuts', 'API::fetchShortcuts');
	$routes->get('fetchDistricts', 'API::fetchDistricts');
    $routes->get('fetchStates', 'API::fetchStates');
	$routes->get('fetchNumberplates', 'API::fetchNumberplates');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
