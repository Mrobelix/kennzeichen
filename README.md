# Kennzeichenliste mit CodeIgniter 4

## Was ist CodeIgniter?

CodeIgniter ist ein PHP-Full-Stack-Web-Framework, das leicht, schnell, flexibel und sicher ist.
Weitere Informationen finden Sie auf der [offizielle Website](http://codeigniter.com).

CodeIgniter 4 enthält einen mit dem Composer installierbaren App-Starter. Es wurde aus dem [Entwicklungs-Repository](https://github.com/codeigniter4/CodeIgniter4) erstellt.

Weitere Informationen zu den Plänen für CodeIgniter 4 finden Sie in [der Ankündigung](http://forum.codeigniter.com/thread-62615.html) im Forum.

Das Benutzerhandbuch zu dieser Version des Frameworks finden Sie
[hier](https://codeigniter4.github.io/userguide/).

## Installation und Aktualisierungen

`composer create-project codeigniter4/appstarter` und dann `composer update` wenn es eine neue Version des Frameworks gibt.

Überprüfen Sie bei der Aktualisierung die Versionshinweise, um zu sehen, ob es irgendwelche Änderungen gibt, die Sie
auf Ihren `app`-Ordner anwenden müssen. Die betroffenen Dateien können kopiert oder zusammengeführt werden von `vendor/codeigniter4/framework/app`.

## Einrichtung

Kopieren Sie `env` nach `.env` und passen Sie es für Ihre Anwendung an, insbesondere die baseURL
und alle Datenbankeinstellungen.

## Wichtige Änderung der index.php

`index.php` befindet sich nicht mehr im Stammverzeichnis des Projekts! Er wurde in den Ordner `public` verschoben, um die Sicherheit und die Trennung der Komponenten zu verbessern.

Das bedeutet, dass Sie Ihren Webserver so konfigurieren sollten, dass er auf den `public`-Ordner Ihres Projekts verweist, und nicht und nicht auf das Stammverzeichnis des Projekts. Besser wäre es, einen virtuellen Host so zu konfigurieren, dass er dorthin verweist. Eine schlechte Praxis wäre es, Ihren Webserver auf das Stammverzeichnis des Projekts zu verweisen und zu erwarten, dass Sie `public/...` eingeben, da der Rest Ihrer Logik und des
Rahmen offengelegt werden.

**Bitte** lesen Sie das Benutzerhandbuch, um die Funktionsweise von CI4 besser zu verstehen!

## Implementieren der Kennzeichenliste

Nachdem ein frisches CodeIgniter 4 aufgesetzt wurde, muss nun nurnoch der `app`-Ordner und der `public`-Ordner mit dem Ordern hier aus dem Repository ausgetauscht werden. Damit sind alle Controller, Models, Views und alles weitere von der Kennzeichenliste implementiert.

## Implementiertung der Datenbank

Natürlich fehlt jetzt noch eine Datenbank, wie in der **Einrichtung** erklärt. Einfach in die `.env`-Datei folgende Zeilen einfügen:

```.env
database.default.hostname = [DATABASE HOSTNAME oder IP]
database.default.database = [DATABASE NAME]
database.default.username = [DATABASE USER]
database.default.password = [DATABASE PASSWORT]
database.default.DBDriver = MySQLi
```

Eine Test-Datenbank mit allen Deutschen Kennzeichen (Stand Mai 2022) ist im Repository hinterlegt und kann einfach importiert werden. (`database.sql`)

Datenbank Struktur:

**users**
```
id: int(11)
name: varchar(255)
email: varchar(255)
password: varchar(255)
token: varchar(32)
permission: int(2)
create_date: datetime
last_login: datetime
```

**numberplates**
```
shortcut: varchar(3)
origin: varchar(255)
district: varchar(255)
state: varchar(2)
```

**states**
```
shortcut: varchar(2)
state: varchar(255)
```

## Server-Anforderungen

PHP Version 7.3 oder höher ist erforderlich, mit den folgenden Erweiterungen installiert:

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) wenn Sie vorhaben, die HTTP\CURLRequest-Bibliothek zu verwenden

Stellen Sie außerdem sicher, dass die folgenden Erweiterungen in Ihrem PHP aktiviert sind:

- json (standardmäßig aktiviert - nicht ausschalten)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (standardmäßig aktiviert - nicht ausschalten)
